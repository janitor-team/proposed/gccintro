#include <math.h>
#include <stdio.h>

int
main (void)
{
  int i;
  double sum = 0;

  for (i = 0; i < 1e7; i++)
    sum += (i + 0.5) * (i + 0.5);

  printf ("sum = %g\n", sum);

  return 0;
}
