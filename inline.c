#include <stdio.h>

double
sq (double x)
{
  return x * x;
}

int
main (void)
{
  int i; 
  double sum = 0;
  for (i = 0; i < 100000000; i++)
      sum += sq (i + 0.5);
  return 0;
}
