#include <iostream>
#include "buffer.h"
#include "tprint.h"

using namespace std;

int 
main ()
{
  Buffer<float> f(10);
  f.insert (0.23);
  f.insert (1.0 + f.get());
  print (f);
  return 0;
}
