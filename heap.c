void
swap (double *base, int i, int j)
{
  double tmp = base[i];
  base[i] = base[j];
  base[j] = tmp;
}

void
downheap (double *data, const int N, int k)
{
  while (k <= N / 2)
    {
      int j = 2 * k;
      if (j < N && (data[j] < data[j + 1]))
        j++;
      if (data[k] < data[j])
        swap (data, j, k);
      else
        break;
      k = j;
    }
}

void
heapsort (double *data, int count)
{
  int N = count - 1, k = (N / 2) + 1;
  do { downheap (data, N, --k); } while (k > 0);
  while (N > 0) {
      swap (data, 0, N);
      downheap (data, --N, 0);
  }
}

#include <stdlib.h>
#include <stdio.h>

int
main (void)
{
  int i, N = 1000000;
  double *x = malloc (N * sizeof (double));

  for (i = 0; i < N; i++)
    x[i] = N / (1.0 + i);

  heapsort (x, N);

  for (i = 0; i < N; i++)
    printf ("%d %g\n", i, x[i]);
}
