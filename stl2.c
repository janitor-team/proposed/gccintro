#include <list>
#include <string>
#include <iostream>

using namespace std;

void foo();

int
main ()
{
  list<string> list;

  list.push_front("Hello");
  list.push_back("World");

  cout << "List size = " << list.size() << endl;
  foo();
  return 0;
}
