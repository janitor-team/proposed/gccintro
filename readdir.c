#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>

int
main (void) 
{
  DIR * d = opendir(".");
  struct dirent * f;

  while ((f = readdir(d)) != 0) 
    {
      printf("%s\n", f->d_name);
    }

  return closedir(d);
}
  
