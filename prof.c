#include <stdio.h>

double
f (int i)
{
  if (i == 1)
    return i;
  return i * f (i - 1);
}

double
g (int i)
{
  double p = 1;
  int k;
  for (k = 1; k <= i; k++)
    p *= k;
  return p;
}


int
main (void)
{
  printf ("f(30) = %g\n", f (30));
  printf ("g(30) = %g\n", g (30));
  return 0;
}
