template<class T> class Grid {
public:
  Grid(int x, int y);
  void set(int x, int y, T f);
  T get(int x, int y);
private:
  int nx;
  int ny;
  T * pT;
};

template<class T> Grid<T>::Grid(int x, int y) {
  nx = x;
  ny = y;
  pT = new T[nx * ny];
};

template<class T> void Grid<T>::set(int x, int y, T f) {
  pT[x*ny+y] = f;
};

template<class T> T Grid<T>::get(int x, int y) {
  return pT[x*ny+y];
};
