#include <iostream>
#include "buffer.h"

using namespace std;

int
main ()
{
  Buffer<float> f(10);
  f.insert (0.25);
  f.insert (1.0 + f.get(0));
  cout << "stored value 0 = " << f.get(0) << endl;
  cout << "stored value 1 = " << f.get(1) << endl;
  return 0;
}
